McKesson Reimbursement Manager
==============================

>Project: REM-TEST  
>Description: Reimbursement Manager Test Applications and Resources  

> [Reimbursement Manager Product Development Home](https://mhsprod.jira.com/wiki/display/RMPD/Reimbursement+Manager+Product+Development+Home)
> 
>##### Required SCM / Build Tools
>* [Git](http://git-scm.com/download)
>* [Maven](http://maven.apache.org/)
>* [Java 7 JDK](http://www.oracle.com/technetwork/java/javase/overview/index.html)

>##### Top-Level Project Elements
>* /fitnesse - Fitnesse testing framework

>##### Developer Notes
>* Refer to README's in each project element for build instructions.  
